﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication7.Models
{
    public class User
    {
        public String Name { get; set; }
        public Email email { get; set; }
        public Phone phone { get; set; }
        public Address address { get; set; }
    }
}